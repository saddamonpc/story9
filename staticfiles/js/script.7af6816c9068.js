function bookSearch() {
    var search = document.getElementById('search').value
    document.getElementById('results').innerHTML = ""
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",
        success: function (data) {
            try {
                results.innerHTML += "<table id='myTable' class='table table-bordered table-hover text-center' style='border: 2px solid #35526F; max-height: 35vh; overflow-y: scroll; margin-bottom: 5px;'><thead style='border: 2px solid #35526F; background-color: #e3f2fd; color: #35526F; text-shadow: none;'><tr><th scope='col' style='border: 1px solid #35526F;'>Title</th><th scope='col' style='border: 1px solid #35526F;'>Author</th><th scope='col' style='border: 1px solid #35526F;'>Likes</th><th scope='col' style='border: 1px solid #35526F;'>Upvote</th></tr></thead><tbody style='border: 2px solid #35526F; text-align: center; color: #35526F; background-color: white;' id='bookTable'></tbody></table>"
                for (i = 0; i < data.items.length; i++) {
                    try {
                        bookTable.innerHTML += "<tr>" +
                            "<td id='title" + i + "' style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.title + "</td>" +
                            "<td id='author" + i + "' style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.authors[0] + "</td>" +
                            "<td id='likes" + i + "' style='font-weight: normal; border: 1px solid #35526F;'>0</td>" +
                            "<td style='font-weight: normal; border: 1px solid #35526F;'><button id='likeButton" + i + "' onclick=like(" + i + ")>↑</button></td></tr>"
                    } catch (TypeError) {
                        bookTable.innerHTML += "<tr>" +
                            "<td id='title" + i + "' style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.title + "</td>" +
                            "<td id='author" + i + "' style='font-weight: normal; border: 1px solid #35526F;'><em>Author Not Found</em></td>" +
                            "<td id='likes" + i + "' style='font-weight: normal; border: 1px solid #35526F;'>0</td>" +
                            "<td style='font-weight: normal; border: 1px solid #35526F;'><button id='likeButton" + i + "' onclick=like(" + i + ")>↑</button></td></tr>"
                    }
                }
                buttons.innerHTML += "<button type='button' id='buttons' class='btn btn-primary' data-toggle='modal'data-target='#exampleModalCenter' onclick=mostLiked()>Most Liked Books</button>"
                break;
            } catch (TypeError) {
                results.innerHTML += "<center><h1 style='color: #35526F; font-family: Lucida Sans Unicode, Lucida Grande, sans-serif; font-size: 1.5vw;'>Did not find any books matching your search results. Please try again.</h1></center"
            }
        },
        type: 'GET'
    })
}

function like(i) {
    var likesForCurrentBook = parseInt(document.getElementById('likes' + i).textContent)
    document.getElementById('likes' + i).innerHTML = likesForCurrentBook + 1
}

document.getElementById('search').addEventListener('keypress', function (e) {
    if (e.keyCode == 13) {
        bookSearch()
    }
}, false)
document.getElementById('searchButton').addEventListener('click', bookSearch, false)