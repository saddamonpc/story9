var number = 1;

$("#searchBtn").click(function () {
    search();
});

function search() {
    var searchValue = $("input").val();
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + searchValue,
        method: "GET"
    }).done(function (data) {
        var books = data.items;
        showBooks(books);
    });
}

function showBooks(books) {
    $.each(books, function (homepage, value) {
        var htmlString = renderHtml(homepage, value);
        $("#bookTable tr:last").after(htmlString);
    });
}

function renderHtml(homepage, value) {
    var numberCol = number++;
    var volume = value.volumeInfo;
    var imageCol = "<img src=" + volume.imageLinks.thumbnail + ">";
    var titleCol = volume.title;
    var authorCol = volume.authors[0];
    return `<tr> 
                <td>` + numberCol + `</td>
                <td>` + imageCol + `</td>
                <td>` + titleCol + `</td>
                <td>` + authorCol + `</td>
                <td style="font-weight: normal; border: 1px solid #35526F;">
                        <center><a href="{% url 'tugas:homepage'"><img
                                    src="{% static 'Like.png'%}" style="width: 2vw; height: 2vw;"></a></center>
                    </td>
             </tr>`
}