function bookSearch() {
    var search = document.getElementById('search').value
    document.getElementById('results').innerHTML = ""
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",
        success: function (data) {
            results.innerHTML += "<table class='table table-bordered table-hover text-center' style='border: 2px solid #35526F; max-height: 35vh; overflow-y: scroll; margin-bottom: 5px;'><thead style='border: 2px solid #35526F; background-color: #e3f2fd; color: #35526F; text-shadow: none;'><tr><th scope='col' style='border: 1px solid #35526F;'>Title</th><th scope='col' style='border: 1px solid #35526F;'>Author</th><th scope='col' style='border: 1px solid #35526F;'>Likes</th><th scope='col' style='border: 1px solid #35526F;'>Upvote</th></tr></thead><tbody style='border: 2px solid #35526F; text-align: center; color: #35526F; background-color: white;' id='bookTable'></tbody></table>"
            for (i = 0; i < data.items.length; i++) {
                bookTable.innerHTML += "<tr>" +
                    "<td style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.title + "</td>" +
                    "<td style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.authors[0] + "</td>" +
                    "<td id='likes" + i + "' style='font-weight: normal; border: 1px solid #35526F;'>0</td>" +
                    "<td style='font-weight: normal; border: 1px solid #35526F;'><button id='likeButton" + i + "' onclick=like(" + i + ")><img src='{% static + " + "Like.png" + " %}'></button></td></tr>"
            }
            results.innerHTML += "<div id='buttons'><button type='button' id='buttons' class='btn btn-primary' data-toggle='modal'data-target='#exampleModalCenter'>Most Liked Books</button></div>"
        },
        type: 'GET'
    })
}

function like(i) {
    var likesForCurrentBook = parseInt(document.getElementById('likes' + i).textContent)
    document.getElementById('likes' + i).innerHTML = likesForCurrentBook + 1
}


{
    /* <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            ...
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div> */
}

document.getElementById('search').addEventListener('keypress', function (e) {
    if (e.keyCode == 13) {
        bookSearch()
    }
}, false)
document.getElementById('button').addEventListener('click', bookSearch, false)