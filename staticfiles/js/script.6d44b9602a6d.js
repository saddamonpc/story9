function bookSearch() {
    var search = document.getElementById('search').value
    document.getElementById('bookTable').innerHTML = ""
    console.log(search)

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",
        success: function (data) {
            for (i = 0; i < data.items.length; i++) {
                bookTable.innerHTML += "<tr><td style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].industryIdentifiers[0].identifier + "</td><td style='font-weight: normal; border: 1px solid #35526F;'><a href='" + data.items[i].buyLink + "' style='color: #35526F'>" + data.items[i].volumeInfo.title + "</a></td><td style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.authors[0] + "</td></tr>"
            }
        },
        type: 'GET'
    })
}

document.getElementById('search').addEventListener('keypress', function (e) {
    if (e.keyCode == 13) {
        bookSearch()
    }
}, false)
document.getElementById('button').addEventListener('click', bookSearch, false)


{
    /* <td>
                            <center><a href="{% url 'tugas:deleteItem' name=friend.name %}"><img
                                        src="{% static 'Delete.png'%}" style="width: 2vw; height: 2vw;"></a></center>
                        </td> */
}