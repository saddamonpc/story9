function bookSearch() {
    var search = document.getElementById('search').value
    document.getElementById('results').innerHTML = ""
    document.getElementById('buttons').innerHTML = ""
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",
        success: function (data) {
            try {
                var temp = data.items.length - 1;
                results.innerHTML += "<table id='myTable' class='table table-bordered table-hover text-center' style='border: 2px solid #35526F; max-height: 35vh; overflow-y: scroll; margin-bottom: 5px;'><thead style='border: 2px solid #35526F; background-color: #e3f2fd; color: #35526F; text-shadow: none;'><tr><th scope='col' style='border: 1px solid #35526F;'>Title</th><th scope='col' style='border: 1px solid #35526F;'>Author</th><th scope='col' style='border: 1px solid #35526F;'>Likes</th><th scope='col' style='border: 1px solid #35526F;'>Upvote</th></tr></thead><tbody style='border: 2px solid #35526F; text-align: center; color: #35526F; background-color: white;' id='bookTable'></tbody></table>"
                for (i = 0; i < data.items.length; i++) {
                    try {
                        bookTable.innerHTML += "<tr>" +
                            "<td id='title" + i + "' style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.title + "</td>" +
                            "<td id='author" + i + "' style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.authors[0] + "</td>" +
                            "<td id='likes" + i + "' style='font-weight: normal; border: 1px solid #35526F;'>0</td>" +
                            "<td style='font-weight: normal; border: 1px solid #35526F;'><button id='likeButton" + i + "' onclick=like(" + i + ")>↑</button></td></tr>"
                    } catch (TypeError) {
                        bookTable.innerHTML += "<tr>" +
                            "<td id='title" + i + "' style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.title + "</td>" +
                            "<td id='author" + i + "' style='font-weight: normal; border: 1px solid #35526F;'><em>Author Not Found</em></td>" +
                            "<td id='likes" + i + "' style='font-weight: normal; border: 1px solid #35526F;'>0</td>" +
                            "<td style='font-weight: normal; border: 1px solid #35526F;'><button id='likeButton" + i + "' onclick=like(" + i + ")>↑</button></td></tr>"
                    }
                    buttons.innerHTML += "<button id='buttons' class='btn btn-primary' data-toggle='modal'data-target='#exampleModalCenter' onclick=mostLiked()>Most Liked Books</button>"
                }
            } catch (TypeError) {
                results.innerHTML += "<center><h1 style='color: #35526F; font-family: Lucida Sans Unicode, Lucida Grande, sans-serif; font-size: 1.5vw;'>Did not find any books matching your search results. Please try again.</h1></center"
            }
        },
        type: 'GET'
    })
}

function like(i) {
    var likesForCurrentBook = parseInt(document.getElementById('likes' + i).textContent)
    document.getElementById('likes' + i).innerHTML = likesForCurrentBook + 1
}

function sortTable() {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("myTable");
    switching = true;
    while (switching) {
        switching = false;
        rows = table.rows;
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName("TD")[2];
            y = rows[i + 1].getElementsByTagName("TD")[2];
            if (parseInt(x.textContent) < parseInt(y.textContent)) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

function mostLiked() {
    document.getElementById('modal').innerHTML = ""
    sortTable()
    var table = document.getElementById("myTable");
    if (parseInt(table.rows[1].getElementsByTagName("TD")[2].textContent) === 0) {
        modal.innerHTML += "<div class='modal fade' id='exampleModalCenter' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true'><div class='modal-dialog modal-dialog-centered' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' id='exampleModalLongTitle'>Most Liked Books</h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'>None of the displayed books have any likes.</div><div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal' style='background-color: #35526F; color: #ffffff;'>Close</button></div></div></div></div>"
    }
    if (table.rows.length === 2) {
        modal.innerHTML += "<div class='modal fade' id='exampleModalCenter' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true'><div class='modal-dialog modal-dialog-centered' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' id='exampleModalLongTitle'>Most Liked Books</h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'>1. <strong>" +
            table.rows[1].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[1].getElementsByTagName("TD")[1].textContent + ".</div><div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal' style='background-color: #35526F; color: #ffffff;'>Close</button></div></div></div></div>"
    } else if (table.rows.length === 3) {
        modal.innerHTML += "<div class='modal fade' id='exampleModalCenter' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true'><div class='modal-dialog modal-dialog-centered' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' id='exampleModalLongTitle'>Most Liked Books</h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'>1. <strong>" +
            table.rows[1].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[1].getElementsByTagName("TD")[1].textContent + ".<br />2. <strong>" +
            table.rows[2].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[2].getElementsByTagName("TD")[1].textContent + ".</div><div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal' style='background-color: #35526F; color: #ffffff;'>Close</button></div></div></div></div>"
    } else if (table.rows.length === 4) {
        modal.innerHTML += "<div class='modal fade' id='exampleModalCenter' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true'><div class='modal-dialog modal-dialog-centered' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' id='exampleModalLongTitle'>Most Liked Books</h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'>1. <strong>" +
            table.rows[1].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[1].getElementsByTagName("TD")[1].textContent + ".<br />2. <strong>" +
            table.rows[2].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[2].getElementsByTagName("TD")[1].textContent + ".<br />3. <strong>" +
            table.rows[3].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[3].getElementsByTagName("TD")[1].textContent + ".</div><div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal' style='background-color: #35526F; color: #ffffff;'>Close</button></div></div></div></div>"
    } else if (table.rows.length === 5) {
        modal.innerHTML += "<div class='modal fade' id='exampleModalCenter' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true'><div class='modal-dialog modal-dialog-centered' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' id='exampleModalLongTitle'>Most Liked Books</h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'>1. <strong>" +
            table.rows[1].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[1].getElementsByTagName("TD")[1].textContent + ".<br />2. <strong>" +
            table.rows[2].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[2].getElementsByTagName("TD")[1].textContent + ".<br />3. <strong>" +
            table.rows[3].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[3].getElementsByTagName("TD")[1].textContent + ".<br />4. <strong>" +
            table.rows[4].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[4].getElementsByTagName("TD")[1].textContent + ".</div><div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal' style='background-color: #35526F; color: #ffffff;'>Close</button></div></div></div></div>"
    } else {
        modal.innerHTML += "<div class='modal fade' id='exampleModalCenter' tabindex='-1' role='dialog' aria-labelledby='exampleModalCenterTitle' aria-hidden='true'><div class='modal-dialog modal-dialog-centered' role='document'><div class='modal-content'><div class='modal-header'><h5 class='modal-title' id='exampleModalLongTitle'>Most Liked Books</h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body'>1. <strong>" +
            table.rows[1].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[1].getElementsByTagName("TD")[1].textContent + ".<br />2. <strong>" +
            table.rows[2].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[2].getElementsByTagName("TD")[1].textContent + ".<br />3. <strong>" +
            table.rows[3].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[3].getElementsByTagName("TD")[1].textContent + ".<br />4. <strong>" +
            table.rows[4].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[4].getElementsByTagName("TD")[1].textContent + ".<br />5. <strong>" +
            table.rows[5].getElementsByTagName("TD")[0].textContent + "</strong> by " + table.rows[5].getElementsByTagName("TD")[1].textContent + ".</div><div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal' style='background-color: #35526F; color: #ffffff;'>Close</button></div></div></div></div>"
    }
}

document.getElementById('search').addEventListener('keypress', function (e) {
    if (e.keyCode == 13) {
        bookSearch()
    }
}, false)
document.getElementById('searchButton').addEventListener('click', bookSearch, false)