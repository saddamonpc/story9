function bookSearch() {
    var search = document.getElementById('search').value
    document.getElementById('results').innerHTML = ""
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",
        success: function (data) {
            results.innerHTML = "<table class='table table-bordered table-hover text-center' style='border: 2px solid #35526F; max-height: 35vh; overflow-y: scroll; margin-bottom: 15%;'><thead style='border: 2px solid #35526F; background-color: #e3f2fd; color: #35526F; text-shadow: none;'><tr><th scope='col' style='border: 1px solid #35526F;'>Title</th><th scope='col' style='border: 1px solid #35526F;'>Author</th><th scope='col' style='border: 1px solid #35526F;'>Likes</th><th scope='col' style='border: 1px solid #35526F;'>Upvote</th></tr></thead><tbody style='border: 2px solid #35526F; text-align: center; color: #35526F; background-color: white;' id='bookTable'></tbody></table>"
            for (i = 0; i < data.items.length; i++) {
                results.innerHTML += "<tr>" +
                    "<td style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.title + "</td>" +
                    "<td style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.authors[0] + "</td>" +
                    "<td id='likes" + i + "' style='font-weight: normal; border: 1px solid #35526F;'>0</td>" +
                    "<td style='font-weight: normal; border: 1px solid #35526F;'><button id='likeButton" + i + "' onclick=like(" + i + ")><img src='{% static 'Like.png'%}'></button></td></tr>"
            }
        },
        type: 'GET'
    })
}

document.getElementById('search').addEventListener('keypress', function (e) {
    if (e.keyCode == 13) {
        bookSearch()
    }
}, false)
document.getElementById('button').addEventListener('click', bookSearch, false)