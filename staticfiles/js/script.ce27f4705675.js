function bookSearch() {
    var search = document.getElementById('search').value
    document.getElementById('results').innerHTML = ""
    console.log(search)

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",
        success: function (data) {
            for (i = 0; i < data.items.length; i++) {
                bookTable.innerHTML += "<td style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.title + "</td>"
                bookTable.innerHTML += "<td style='font-weight: normal; border: 1px solid #35526F;'>" + data.items[i].volumeInfo.author + "</td>"
            }
        },
        type: 'GET'
    })
}

document.getElementById('search').addEventListener('keypress', function (e) {
    if (e.keyCode == 13) {
        bookSearch()
    }
}, false)
document.getElementById('button').addEventListener('click', bookSearch, false)