from django.test import TestCase
from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import *
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Unit Testing
class Story9Test (TestCase):
    def testApps(self):
        self.assertEqual(TugasConfig.name, 'tugas')
        self.assertEqual(apps.get_app_config('tugas').name, 'tugas')

    def testHomepageURL(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def testHomepageUsingTemplate(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')

    def testHomepageUsingHomepageFunction(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

# Functional Testing
class Story9FunctionalTest(unittest.TestCase):
    def setUp(self):
        chromeOptions = Options()
        chromeOptions.add_argument('--no-sandbox')
        chromeOptions.add_argument("--headless")
        self.browser = webdriver.Chrome(options=chromeOptions)

    def tearDown(self):
        self.browser.quit()

    def testIfSearchReturnsTable(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.browser.find_element_by_id("search").send_keys("Harry Potter")
        self.browser.find_element_by_id("searchButton").click()
        time.sleep(5)
        self.assertIn("Fantastic Beasts and Where to Find Them", self.browser.find_element_by_id("title0").text) and self.assertIn("J.K. Rowling", self.browser.find_element_by_id("author0").text)  

    def testIfSearchDoesNotReturnTable(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.browser.find_element_by_id("search").send_keys("kwjekqw")
        self.browser.find_element_by_id("searchButton").click()
        time.sleep(5)
        self.assertIn("Did not find any books matching your search results. Please try again.", self.browser.page_source)

    def testIfLikeButtonWorks(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.browser.find_element_by_id("search").send_keys("Harry Potter")
        self.browser.find_element_by_id("searchButton").click()
        time.sleep(5)
        numberOfLikes = self.browser.find_element_by_id("likes0").text
        self.assertIn("0", numberOfLikes)
        self.browser.find_element_by_id("likeButton0").click()
        numberOfLikes = self.browser.find_element_by_id("likes0").text
        self.assertIn("1", numberOfLikes)

    def testIfModalWorks(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.browser.find_element_by_id("search").send_keys("Harry Potter")
        self.browser.find_element_by_id("searchButton").click()
        time.sleep(5)
        self.browser.find_element_by_id("likeButton0").click()
        self.browser.find_element_by_id("likeButton1").click()
        self.browser.find_element_by_id("likeButton1").click()
        self.browser.find_element_by_id("likeButton2").click()
        self.browser.find_element_by_id("likeButton2").click()
        self.browser.find_element_by_id("likeButton2").click()
        self.browser.find_element_by_id("likeButton3").click()
        self.browser.find_element_by_id("likeButton3").click()
        self.browser.find_element_by_id("likeButton3").click()
        self.browser.find_element_by_id("likeButton3").click()
        self.browser.find_element_by_id("likeButton4").click()
        self.browser.find_element_by_id("likeButton4").click()
        self.browser.find_element_by_id("likeButton4").click()
        self.browser.find_element_by_id("likeButton4").click()
        self.browser.find_element_by_id("likeButton4").click()
        time.sleep(5)
        element = self.browser.find_element_by_id('modalButton')
        self.browser.execute_script("arguments[0].click();", element)
        first = "1. " + self.browser.find_element_by_id("title4").text + " by " + self.browser.find_element_by_id("author4").text + "."
        second = "2. " + self.browser.find_element_by_id("title3").text + " by " + self.browser.find_element_by_id("author3").text + "."
        third = "3. " + self.browser.find_element_by_id("title2").text + " by " + self.browser.find_element_by_id("author2").text + "."
        fourth = "4. " + self.browser.find_element_by_id("title1").text + " by " + self.browser.find_element_by_id("author1").text + "."
        fifth = "5. " + self.browser.find_element_by_id("title0").text + " by " + self.browser.find_element_by_id("author0").text + "."
        self.assertIn("1. Harry Potter and the Philosopher's Stone by J K Rowling.", first)
        self.assertIn("2. Harry Potter and the Sorcerer's Stone by J. K. Rowling.", second)
        self.assertIn("3. Harry Potter: The Complete Collection (1-7) by J.K. Rowling.", third)
        self.assertIn("4. Harry Potter and the Goblet of Fire by J. K. Rowling.", fourth)
        self.assertIn("5. Fantastic Beasts and Where to Find Them by J.K. Rowling.", fifth)



    




